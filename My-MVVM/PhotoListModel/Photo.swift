//
//  Photo.swift
//  My-MVVM
//
//  Created by Piotr Błachewicz on 13.04.2018.
//  Copyright © 2018 Piotr Błachewicz. All rights reserved.
//

import Foundation

struct Photos: Codable {
    let photos: [Photo]
}

struct Photo: Codable {
    let id: Int
    let title: String
    let image_url: String
}
