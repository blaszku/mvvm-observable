//
//  PhotoListTableCell.swift
//  My-MVVM
//
//  Created by Piotr Błachewicz on 16.04.2018.
//  Copyright © 2018 Piotr Błachewicz. All rights reserved.
//

import UIKit

class PhotoListTableViewCell: UITableViewCell, ReusableCell {

    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
