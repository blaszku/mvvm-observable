//
//  PhotoListViewController.swift
//  My-MVVM
//
//  Created by Piotr Błachewicz on 13.04.2018.
//  Copyright © 2018 Piotr Błachewicz. All rights reserved.
//

import Foundation
import SDWebImage

class PhotoListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let dataSource = GenericDatSource<PhotoListCellViewModel>()
    lazy var viewModel: PhotoListViewModel = {
        return PhotoListViewModel(dataSource: dataSource)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UINib(nibName: PhotoListTableViewCell.reuseIdentifier, bundle: nil), forCellReuseIdentifier: PhotoListTableViewCell.reuseIdentifier)
        self.initViewModel()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func initViewModel() {
        let observer: Observer? = Observer()
        viewModel.dataSource?.data.addObserver(observer!, options: [.initial, .new]) { [weak self] (photos, change) in
            self?.tableView.reloadData()
        }
        viewModel.fetchPhotos()
    }
}

extension PhotoListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: PhotoListTableViewCell.reuseIdentifier, for: indexPath) as? PhotoListTableViewCell else {
            return UITableViewCell()
        }
        
        if indexPath.row % 2 == 0 {
            cell.backgroundColor = UIColor.red
        } else {
            cell.backgroundColor = UIColor.blue
        }
        
        let cellViewModel = viewModel.getCellViewModel(at: indexPath)
        
        cell.idLabel.text = cellViewModel?.idText
        cell.titleLabel.text = cellViewModel?.titleText
        if let imageUrl = cellViewModel?.imageUrl {
            cell.imgView.sd_setImage(with: URL(string: imageUrl), completed: nil)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
}
