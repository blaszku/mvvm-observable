//
//  PhotoListViewModel.swift
//  My-MVVM
//
//  Created by Piotr Błachewicz on 13.04.2018.
//  Copyright © 2018 Piotr Błachewicz. All rights reserved.
//

import Foundation

class PhotoListViewModel {
    //MARK: Closures
//    var reloadTableViewClosure: (()->())?
    weak var dataSource: GenericDatSource<PhotoListCellViewModel>?
    
    //MARK: Properties
    init(dataSource: GenericDatSource<PhotoListCellViewModel>) {
        self.dataSource = dataSource
    }
    
    var numberOfCells: Int {
        if let data = dataSource?.data.value {
            return data.count
        }
        return 0
    }
    
    //MARK: Cell VM
    func createCellViewModel(photo: Photo) -> PhotoListCellViewModel {
        //custom cell setup
        
        //
        return PhotoListCellViewModel(idText: String(photo.id), titleText: photo.title, imageUrl: photo.image_url)
    }
    
    func getCellViewModel(at indexPath: IndexPath) -> PhotoListCellViewModel?{
        return dataSource?.data.value[indexPath.row]
    }
    
    //MARK: Photos
    func fetchPhotos() {
        NetworkAPIService.shared.getPhotos(success: { [weak self] (photos) in
            self?.processFetchedPhoto(photos: photos)
        }) { (statusCode, errorMessage) in
            print("[getPhotos] error: \(statusCode),\n\(errorMessage)\n")
        }
    }
    
    private func processFetchedPhoto(photos: [Photo]) {
        var cellViewModels = [PhotoListCellViewModel]()
        for photo in photos {
            cellViewModels.append(createCellViewModel(photo: photo))
        }
        
        self.dataSource?.data.value = cellViewModels
    }
}

