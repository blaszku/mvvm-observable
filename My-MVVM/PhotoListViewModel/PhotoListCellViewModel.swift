//
//  PhotoListCellViewModel.swift
//  My-MVVM
//
//  Created by Piotr Błachewicz on 13.04.2018.
//  Copyright © 2018 Piotr Błachewicz. All rights reserved.
//

import Foundation

struct PhotoListCellViewModel {
    let idText: String
    let titleText: String
    let imageUrl: String
}
