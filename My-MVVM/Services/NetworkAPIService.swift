//
//  NetworkAPIService.swift
//  My-MVVM
//
//  Created by Piotr Błachewicz on 13.04.2018.
//  Copyright © 2018 Piotr Błachewicz. All rights reserved.
//

import Foundation
import Alamofire

class NetworkAPIService: NSObject {
    static let shared = NetworkAPIService()
    
    let baseURL = "https://jsonplaceholder.typicode.com/photos"
    
    func getPhotos(success: @escaping([Photo]) -> Void, failure: @escaping(_ statusCode: Int, _ error: String) -> Void) {
        
    
        
        Alamofire.request(baseURL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate(statusCode: 200..<300)
            .responseJSON { (response) in
                var photos = [Photo]()
                
                switch response.result {
                case.success:
                    if let JSON = response.result.value as? [[String : Any]] {
                        for photo in JSON {
                            if let photoId = photo["id"] as? Int,
                                let photoTitle = photo["title"] as? String,
                                let imageUrl = photo["thumbnailUrl"] as? String {
                                photos.append(Photo(id: photoId, title: photoTitle, image_url: imageUrl))
                            }
                        }
                    }
                    success(photos)
                case .failure(let error):
                    let errorData = NetworkAPIService.parseErrorAndResponse(error, response)
                    failure(errorData.statusCode, errorData.message)
                }
        }
    }
}

//MARK: - Error Parser
extension NetworkAPIService {
    static private func parseErrorAndResponse(_ error: Error, _ response: DataResponse<Any>) -> (statusCode: Int, message: String) {
        var statusCode: Int = error._code
        if let code = response.response?.statusCode {
            statusCode = code
        }
        var message: String = error.localizedDescription
        
        if let data = response.data {
            let errorData = self.getErrorStatusCodeAndMessageFromResponse(data: data)
            
            if let code = errorData.statusCode {
                statusCode = code
            }
            
            if let messageData = errorData.message {
                message = messageData
            }
        }
        return (statusCode, message)
    }
    
    static private func getErrorStatusCodeAndMessageFromResponse(data: Data!) -> (statusCode: Int?, message: String?) {
        var statusCode: Int?
        var message: String?
        
        do {
            let JSON = try JSONSerialization.jsonObject(with: data,
                                                        options: JSONSerialization.ReadingOptions.mutableContainers) as? [String : Any]
            
            if let codeJSON = JSON!["statusCode"] as? Int {
                statusCode = codeJSON
            }
            if let messageJSON = JSON!["error"] as? String {
                message = messageJSON
            }
            return (statusCode, message)
        } catch {
            return (nil, nil)
        }
    }
}
